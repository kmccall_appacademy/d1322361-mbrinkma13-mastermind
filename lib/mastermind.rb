class Code
  attr_reader :pegs

  PEGS = {
    'R' => :red,
    'G' => :green,
    'B' => :blue,
    'Y' => :yellow,
    'O' => :orange,
    'P' => :purple
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def [](i)
    pegs[i]
  end

  def ==(code)
    if !code.is_a?(Code)
      false
    elsif self.pegs == code.pegs
      true
    else
      false
    end
  end

  def self.parse(colors_str)
    pegs = colors_str.split('').map do |ch|
      if PEGS.key?(ch.upcase)
        PEGS[ch.upcase]
      else
        raise "No key #{ch.upcase}"
      end
    end
    Code.new(pegs)
  end

  def self.random
    rand_pegs = []
    4.times { rand_pegs.push(PEGS.values.shuffle[0]) }
    Code.new(rand_pegs)
  end

  def exact_matches(code)
    exact_matches = 0
    pegs.each_index do |i|
      if pegs[i] == code[i]
        exact_matches += 1
      end
    end
    exact_matches
  end

  def near_matches(code)
    near_matches = Hash.new(0)
    pegs.each_index do |i|
      unless exact_match_keys(code).include?(pegs[i])
        if code.pegs.include?(pegs[i])
          near_matches[pegs[i]] += 1
        end
      end
    end
    near_matches.keys.count
  end

  def exact_match_keys(code)
    keys = []
    pegs.each_index do |i|
      if pegs[i] == code[i]
        keys.push(pegs[i])
      end
    end
    keys
  end

end



class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  TURN_LIMIT = 10

  def play
    i = 0
    while i < TURN_LIMIT
      guess = get_guess
      if guess == secret_code
        puts "*** You win! ***"
        return
      else
        display_matches(guess)
      end
      i += 1
      if i == 9
        puts "* #{TURN_LIMIT - i} guess remaining *"
      else
        puts "* #{TURN_LIMIT - i} guesses remaining *"
      end
    end

  end

  def get_guess
    puts "Please enter a four letter color code: "
    Code.parse($stdin.gets.chomp)
  end

  def display_matches(guess)
    exact = @secret_code.exact_matches(guess)
    near = @secret_code.near_matches(guess)
    puts "#{exact} exact matches"
    puts "#{near} near matches"
  end

end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
